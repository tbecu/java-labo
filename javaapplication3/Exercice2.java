package javaapplication3;

import java.util.Scanner;

/**
 *
 * @author Thomas Bécu
 */
public class Exercice2 {
    
    public static void main(String[] args) {
        String mot;
        int cpt=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrée un mot");
        mot = sc.nextLine();
        mot = mot.toLowerCase();
        for (int i = 0; i < mot.length(); i++) {
            switch(mot.charAt(i)){
                case 'a':
                    cpt++;
                    break;
                case 'o':
                    cpt++;
                    break;
               case 'e':
                    cpt++;
                    break;
              case 'i':
                    cpt++;
                    break;
             case 'u':
                    cpt++;
                    break;
             case 'y':
                    cpt++;
                    break;
        }
       }
        System.out.println("nombre de voyelle:"+cpt);
    }

}
