package javaapplication3;

import java.util.Scanner;

/**
 *
 * @author Thomas Bécu
 */
public class Exercice1 {
    public static void main(String[] args) {
        
        String mot; 
        Scanner sc = new Scanner(System.in);
        do
        {
        System.out.println("Entrée un mot de 8 lettres");
        mot = sc.nextLine();  
        }
        while(mot.length()!=8);
        mot=mot.toUpperCase();
        System.out.println("Votre mot:"+mot);
        mot=mot.replace('A','O');
        System.out.println("Votre mot est maintenant "+mot);
    }
}
