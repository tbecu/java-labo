
package javaapplication3;
import java.util.*;

/**
 * @author Thomas Bécu
 */
public class Main {

    public static void main(String[] args) {
        
        String nom="Bonjour";
        nom = nom.toLowerCase();
        int i;
        i = nom.length();
        System.out.println("Nombre de lettre du mot "+nom+" est "+i);
        
        //Entree une chaine
        System.out.println("Entrée nvotre chaine :");
        Scanner sc = new Scanner(System.in);
        nom = sc.nextLine();
        nom = nom.toLowerCase();
        i = nom.length();
        System.out.println("Nombre de lettre du mot "+nom+" est "+i);
        
    }
}
